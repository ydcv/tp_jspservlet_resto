package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Article;

/**
 * Servlet implementation class lstAjoutArticle
 */
@WebServlet("/add")
public class AjoutArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjoutArticle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("CarteAddServlet Lanc� !");
		String nomArt = "";
		String descArt = "";
		int ArtPrice = 0;
		int ArtQuantity = 0;
		int ArtId = 0;
				
		for(int i = 0; i <= 100;){
			String nom = "nom"+0;
			
		if(request.getParameter("quantity"+i) != null)
		{
			nomArt = request.getParameter("nom"+i);
			descArt = request.getParameter("desc"+i);
			ArtPrice = Integer.parseInt(request.getParameter("price"+i));
			ArtQuantity = Integer.parseInt(request.getParameter("quantity"+i));
			ArtId = Integer.parseInt(request.getParameter("id"+i));
	
		}
		 i++;
		}
	
		
	
		
		ServletContext application = this.getServletContext();
		ArrayList<Article> uList = (ArrayList)application.getAttribute("listePanier");
		
		Article a = new Article(nomArt,descArt,ArtPrice,ArtId,ArtQuantity);
		uList.add(a);
		System.out.println(uList);
		request.setAttribute("listePanier", uList);
		//ArrayList<Article> aListPanier = (ArrayList<Article>) request.getAttribute("listePanier");
		
		request.getRequestDispatcher("carte").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
