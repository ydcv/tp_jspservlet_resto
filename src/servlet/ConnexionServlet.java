package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ClientJdbcMysql;
import model.Client;

/**
 * Servlet implementation class
 */
@WebServlet("/connexionserv")
public class ConnexionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConnexionServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// Pour afficher le message d'erreur qui correspond
		request.setAttribute("erreurLog", "");

		ClientJdbcMysql cjm = new ClientJdbcMysql();

		Client c = null;
		String mdp = request.getParameter("mdp");
		String login = request.getParameter("login");

		try {
			c = cjm.findByLogin(login);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (c == null) {
			request.setAttribute("erreurLog", "Votre login n'est pas reconnu, Veuillez recommencer la saisie");
			request.getRequestDispatcher("WEB-INF/connexion.jsp").forward(request, response);
		} else {

			if (c.getPassword().equals(mdp)) {
				request.setAttribute("curClient", c);
				request.getRequestDispatcher("home.jsp").forward(request, response);
			} else {
				request.setAttribute("erreurLog",
						"Votre mot de passe n'est pas reconnu, Veuillez recommencer la saisie");
				request.getRequestDispatcher("WEB-INF/connexion.jsp").forward(request, response);
			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
