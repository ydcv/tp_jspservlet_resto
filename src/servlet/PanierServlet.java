package servlet;

import java.io.IOException;
import java.util.*;


import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Article;
import model.Panier;

/**
 * Servlet implementation class PanierServ
 */
@WebServlet("/panierserv")
public class PanierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PanierServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			
		ServletContext app = this.getServletContext();
		
		ArrayList<Article> listePanier = (ArrayList<Article>) app.getAttribute("listePanier");
		
		Panier panier = new Panier();
		
		int prixTotal = 0 ;
		for(Article a : listePanier)
		{
		
			prixTotal = prixTotal+(a.getPrice()*a.getQuantity());
			
		}
		panier.setPrixTotal(prixTotal);
		
		request.setAttribute("totalPanier", panier);
		request.setAttribute("listePanier",listePanier);
		request.getRequestDispatcher("WEB-INF/panier.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
