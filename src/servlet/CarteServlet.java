package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ArticleJdbcMysql;
import model.Article;

/**
 * Servlet implementation class CarteServlet
 */
@WebServlet("/carte")
public class CarteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<Article> PanierList = null;
		ArrayList<Article> aList = null;
		if( request.getAttribute("listePanier") != null)
		{
			PanierList = (ArrayList<Article>) request.getAttribute("listePanier");
		
		}else
		{
			PanierList = new ArrayList<Article>();
			
		}
	
		try {
			aList = testSelectAllArticleDao();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("listePanier", PanierList);
		request.setAttribute("Articles",aList);
		request.getRequestDispatcher("WEB-INF/carte.jsp").forward(request, response);
	}
	protected ArrayList<Article> testSelectAllArticleDao() throws ClassNotFoundException, SQLException
	{
		ArticleJdbcMysql jdbc = new ArticleJdbcMysql();
		ArrayList<Article> aList = (ArrayList<Article>) jdbc.findAll();
		return aList;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
