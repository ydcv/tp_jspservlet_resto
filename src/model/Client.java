package model;

public class Client {
	private int id;
	private String name, firstName;
	private String password;
	private String adresse;
	private String login;
	
	public Client(){}

	public Client(String name, String firstName, String password, String adresse, String login) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.password = password;
		this.adresse = adresse;
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", firstName=" + firstName + ", password=" + password
				+ ", adresse=" + adresse + ", login=" + login + "]";
	}
	
	
	
}
