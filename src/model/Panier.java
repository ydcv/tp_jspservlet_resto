package model;

import java.util.ArrayList;

public class Panier {
	
	private int ref;
	private String description;
	private int quantite;
	private int prixUnitaire;
	private int prixTotal;
	
	private int id;
	
	private ArrayList<Article> listeArticle;
	
	
	
	public Panier(int id, ArrayList<Article> listeArticle) {
		super();
		this.id = id;
		this.listeArticle = listeArticle;
	}
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public ArrayList<Article> getListeArticle() {
		return listeArticle;
	}


	public void setListeArticle(ArrayList<Article> listeArticle) {
		this.listeArticle = listeArticle;
	}


	public void setPrixTotal(int prixTotal) {
		this.prixTotal = prixTotal;
	}


	public Panier(int ref, String decription, int quantite, int prixUnitaire, int prixTotal) {
		// TODO Auto-generated constructor stub
	}


	public Panier() {
	}


	public int getRef() {
		return ref;
	}


	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getQuantite() {
		return quantite;
	}



	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}



	public double getPrixUnitaire() {
		return prixUnitaire;
	}



	public void setPrixUnitaire(int prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}



	public int getPrixTotal() {
		return prixTotal;
	}



	public void setTotal(int prixTotal) {
		this.prixTotal = prixTotal;
	}



	public void setRef(int ref) {
		this.ref = ref;
	}


	@Override
	public String toString() {
		return "Panier [ref=" + ref + ", description=" + description + ", quantite=" + quantite + ", prixUnitaire="
				+ prixUnitaire + ", prixTotal=" + prixTotal + ", id=" + id + "]";
	}



	
	

}
