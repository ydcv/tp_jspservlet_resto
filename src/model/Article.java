package model;

public class Article {
	private String name,Desc;
	private int price;
	private int id;
	private int quantity;
	
	public Article()
	{
		
	}


	public Article(String name, String desc, int price, int id,int quantity) {
		super();
		this.name = name;
		Desc = desc;
		this.price = price;
		this.quantity = quantity;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return Desc;
	}

	public void setDesc(String desc) {
		Desc = desc;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	@Override
	public String toString() {
		return "Article [name=" + name + ", Desc=" + Desc + ", price=" + price +" Quantit� ="+quantity+"]";
	}
	
}
