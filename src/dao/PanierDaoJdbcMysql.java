package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Panier;
import model.mysqlConnection;

public class PanierDaoJdbcMysql extends mysqlConnection implements Dao<Panier, Object> {

	@Override
	public Panier findById(Object id) throws ClassNotFoundException, SQLException {
		return null;
	}

	@Override
	public List<Panier> findAll() throws ClassNotFoundException, SQLException {

		ArrayList<Panier> liste = new ArrayList<Panier>();

		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(getUrl(), getUser(), getMdp());
		String sql = "select * from panier";
		// d�claration de la requ�te sur la connexion
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		// execution de la requ�te
		while (rs.next()) {
			Panier p = new Panier();
			p.setRef(rs.getInt(1));
			p.setDescription(rs.getString(2));
			p.setQuantite(rs.getInt(3));
			p.setPrixUnitaire((int)rs.getDouble(4));
			p.setTotal((int)rs.getDouble(5));
			
			liste.add(p);
		}
		conn.close();
		return liste;
	}

	@Override
	public void create(Panier x) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Panier obj) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Panier obj) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

	}

}
