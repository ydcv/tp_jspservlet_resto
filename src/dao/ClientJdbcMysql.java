package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Client;
import model.mysqlConnection;

public class ClientJdbcMysql extends mysqlConnection implements ClientDao {

	public boolean contains(Client c) throws ClassNotFoundException, SQLException {

		return this.findByLogin(c.getLogin()) != null;
	}
	
	public Client findByLogin(String login) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(super.getUrl(), super.getUser(), super.getMdp());
		String sql = "select * from clients where login='" +login +"'";
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		Client a = null;

		while (rs.next()) {
			a = new Client();

			a.setName(rs.getString(2));
			a.setFirstName(rs.getString(3));
			a.setPassword(rs.getString(4));
			a.setAdresse(rs.getString(5));
			a.setLogin(rs.getString(6));

		}
		conn.close();

		return a;
	}
	
	@Override
	public Client findById(Integer id) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(super.getUrl(), super.getUser(), super.getMdp());
		String sql = "select * from clients where id=" +id;
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		Client a = null;

		while (rs.next()) {
			a = new Client();

			a.setId(rs.getInt(1));
			a.setName(rs.getString(2));
			a.setFirstName(rs.getString(3));
			a.setPassword(rs.getString(4));
			a.setAdresse(rs.getString(5));
			a.setLogin(rs.getString(6));

		}
		conn.close();

		return a;
	}

	@Override
	public List<Client> findAll() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(super.getUrl(), super.getUser(), super.getMdp());

		String sql = "select * from clients";

		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		ArrayList<Client> liste = new ArrayList<Client>();

		while (rs.next()) {
			Client a = new Client();
			a.setId(rs.getInt(1));
			a.setName(rs.getString(2));
			a.setFirstName(rs.getString(3));
			a.setPassword(rs.getString(4));
			a.setAdresse(rs.getString(5));
			a.setLogin(rs.getString(6));
			liste.add(a);
		}
		conn.close();

		return liste;
	}

	@Override
	public void create(Client x) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(super.getUrl(), super.getUser(), super.getMdp());
		
		String sql = "insert into clients (name,firstname,password,adresse,login) values (?,?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, x.getName());
		ps.setString(2, x.getFirstName());
		ps.setString(3, x.getPassword());
		ps.setString(4, x.getAdresse());
		ps.setString(5, x.getLogin());
		
		ps.executeUpdate();
		conn.close();
		
	}

	@Override
	public void update(Client a) throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(super.getUrl(), super.getUser(), super.getMdp());
		
		String sql = "update clients set name='" + a.getName() 
		+ "', firstname='"+ a.getFirstName() 
		+"' , password='"+ a.getPassword() 
		+ "', adresse='"+ a.getAdresse()
		+ "' where login='"+ a.getLogin() +"'";
		
		System.out.println(sql);

		PreparedStatement ps = conn.prepareStatement(sql);
		
		ps.executeUpdate();
		conn.close();
		
	}

	@Override
	public void delete(Client obj) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(super.getUrl(), super.getUser(), super.getMdp());
		
		String sql = "delete from clients where id=" + obj.getId();

		Statement st = conn.createStatement();
		st.executeUpdate(sql);
		conn.close();
		
	}

}
