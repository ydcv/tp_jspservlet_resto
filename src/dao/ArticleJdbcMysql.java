package dao;


import java.sql.*;
import java.util.*;

import model.*;

public class ArticleJdbcMysql extends mysqlConnection implements Dao{

	@Override
	public Object findById(Object id) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Article> findAll() throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		ArrayList<Article> liste = new ArrayList<Article>();
		
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(getUrl(),getUser(),getMdp());
		String sql = "select * from articles";
		// d�claration de la requ�te sur la connexion
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);
		// execution de la requ�te
		while(rs.next())
			{
				Article a = new Article();
				a.setId(rs.getInt(1));
				a.setName(rs.getString(2));
				a.setDesc(rs.getString(3));
				a.setPrice((int) rs.getDouble(4));
				liste.add(a);
			}
		conn.close();
		return liste;
	}

	@Override
	public void create(Object x) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Object obj) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Object obj) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		
	}

}
