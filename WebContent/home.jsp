<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:directive.include file="/WEB-INF/template/css.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
</head>
<body>
<jsp:directive.include file="/WEB-INF/template/header.jsp" />
	<div class="container">
		<div class="row header-navigation">
			<div class="col">
				<a class="link-secondary" href="WEB-INF/carte.jsp">Acc�s � la carte</a>
			</div>
		</div>
	</div>
	
		<div class="container">
		<div class="row header-navigation">
			<div class="col">
				<a class="link-secondary" href="loginserv">Acc�s � la connexion</a>
			</div>
		</div>
		</div>
	
			<div class="container">
		<div class="row header-navigation">
			<div class="col">
				<a class="link-secondary" href="panierserv">Acc�s au panier</a>
			</div>
		</div>
	</div>
<jsp:directive.include file="/WEB-INF/template/footer.jsp" />
</body>
</html>