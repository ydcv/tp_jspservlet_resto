
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Fast & Pizza</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
     </button>
     <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Accueil</a>
        </li>
        <li class="nav-item">
        <%
        String lien = "";
      	if(request.getRequestURL().toString().equals("http://localhost:8080/tp_jspservlet_resto/WEB-INF/carte.jsp"))
      	{
      	 	lien = "<a class=\"nav-link nav-active\" href=\"panierserv\"" +" "+""+">"+"Mon panier"+"</a>";
          	out.print(lien);
      	}
      	else
      	{
      	 	lien = "<a class=\"nav-link \" href=\"carte\"" +" "+""+">"+"La carte"+"</a>";
          	out.print(lien);
      	}
        %>
        
        </li>
      </ul>
    </div>
  </div>
</nav>