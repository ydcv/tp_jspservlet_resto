<%

	if(request.getRequestURL().toString().equals("http://localhost:8080/tp_jspservlet_resto/WEB-INF/carte.jsp"))
	{
	 lien = "<footer class=\"navbar navbar-light bg-dark  w-100\">";
  	 out.print(lien);
	}
	else
	{
	  lien = "<footer class=\"navbar navbar-light bg-dark fixed-bottom\">";
  	out.print(lien);
	}
%>
	
  <div class="container">
<ul class="list-group bg-dark text-light">
<li class="list-group-item bg-dark text-light" style="font-weight: bold; color:#fff; font-size: 14px; text-transform: uppercase; letter-spacing: 0; margin-bottom: 5px;">NOS PRODUITS</li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/la-carte">La Carte</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/les-pizzas-signatures">Les Pizzas Signatures</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/nos-produits/recettes-de-pizza">Recettes de Pizza</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/double-kiff">Double Kiff</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/big-one">Big One</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/nos-produits/sauce-tomate-bio">Sauce Tomate Bio</a></li>
</ul>
<ul class="list-group bg-dark text-light">
<li class="list-group-item bg-dark text-light"  style=" font-weight: bold; color: #fff; font-size: 14px; text-transform: uppercase; letter-spacing: 0; margin-bottom: 5px;">FAST & PIZZA</li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/la-marque">La Marque</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/dominos-pizza/la-marque/nos-engagements-dominos">Nos engagements</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/nutriscore">Nutri-Score</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/la-marque/newsroom">Presse</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/eclub">Newsletter&nbsp;</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/dominos-pizza/cookies">Cookies</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/dominos-pizza/donnees-personnelles">Donn�es personnelles</a></li>
</ul>
<ul class="list-group bg-dark text-light">
<li class="list-group-item bg-dark text-light"  style=" font-weight: bold; color: #fff; font-size: 14px; text-transform: uppercase; letter-spacing: 0; margin-bottom: 5px;">NOUS CONTACTER</li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/nous-contacter/service-clients">Service clients</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/nous-contacter/politique-de-divulgation-responsable">Politique de divulgation responsable</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/nous-contacter/preference-center">G�rer mes abonnenements</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/desinscription-email">D�sinscription Newsletter</a></li>
<li class="list-group-item bg-dark text-light" class="title" style=" font-weight: bold; color: #fff; font-size: 14px; text-transform: uppercase; letter-spacing: 0; margin-bottom: 5px;">L'EMPLOI</li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/rejoignez-nous/travailler-chez-dominos">Travailler chez Fast&Pizza</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://carrieres.dominos.fr/comment-devenir-franchise">Devenir Franchise</a></li>
</ul>
<ul class="list-group bg-dark text-light">
<li class="list-group-item bg-dark text-light" style=" font-weight: bold; color: #fff; font-size: 14px; text-transform: uppercase; letter-spacing: 0; margin-bottom: 5px;">CONDITIONS DES OFFRES</li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/nos-offres/offre-du-moment">Offre du moment</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/dominos-programme-fidelite">Fast&Pizza's Fid�lit�</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/cree-ta-pizza">Cr�e ta pizza</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/mardis-jeudis-fous">Mardis &amp; Jeudis Fous</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/super-samedi">Super Samedi</a></li>
<li class="list-group-item bg-dark text-light " style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/dominos-pizza/promos-app">Promo's App</a></li>
<li class="list-group-item bg-dark text-light" style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="https://www.dominos.fr/nos-offres/offre-speciale-livraison">Toc Toc Days Livraison</a></li>
</ul>
<ul class="list-group bg-dark text-light">
<li class="list-group-item bg-dark text-light" style=" font-weight: bold; color: #454545; font-size: 14px; text-transform: uppercase; letter-spacing: 0; margin-bottom: 5px;">Pr�s de chez vous</li>
<li class="list-group-item bg-dark text-light"style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/regions/ile-de-france/paris">Pizzas Paris</a></li>
<li class="list-group-item bg-dark text-light"style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/regions/rhone-alpes/lyon">Pizzas Lyon</a></li>
<li class="list-group-item bg-dark text-light"style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/regions/provence-alpes-cote-d-azur/marseille">Pizzas Marseille</a></li>
<li class="list-group-item bg-dark text-light"style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/regions/nord-pas-de-calais/lille">Pizzas Lille</a></li>
<li class="list-group-item bg-dark text-light"style="margin-bottom: 5px; "><a style="color: #fff; font-size: 12px; letter-spacing: 0; text-transform: initial;" href="/regions/pays-de-la-loire/nantes">Pizzas Nantes</a></li>
</ul>
  </div>
</footer> 
