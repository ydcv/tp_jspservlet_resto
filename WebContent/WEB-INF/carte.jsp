<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:directive.include file="/WEB-INF/template/css.jsp" />
<title>Insert title here</title>
</head>
<body>
<jsp:directive.include file="/WEB-INF/template/header.jsp" />
<h2 class="hTitleCenter">Nos pizza</h2>
<!-- <div id="mainContent"class="container">-->

  <div class="column">
    <div class="col-sm">
    </div>
    <div class="col-sm mainContentCol">
    <div class="container" id="mainContent">
<%

ArrayList<Article> aList = (ArrayList<Article>)request.getAttribute("Articles");
ArrayList<Article> listPanier = (ArrayList<Article>) request.getAttribute("listePanier");



int prixTotal = 0 ;
for(Article a : listPanier)
{

	prixTotal = prixTotal+(a.getPrice()*a.getQuantity());
	
}

//request.setAttribute("totalPanier", panier);

int compteur = 0 ;
String cardPanierRow = "";
for(Article a : aList)
{

String card = "<div class=\"card\">"
+"<img class="+"card-img-top"+" src="+"./img/FR_PBEL_fr_menu_9314.jpg"+">"
+"<div class="+"card-body"+">"
+"<form class=\"article-form\" action=\"add\" method=\"post\" >"
+"<input type=\"nubmer\" class=\"card-text\" value="+a.getId()+""+" name=\"id"+compteur+"\" style=\"display:none;\" >"+"</input>"
+"<h5 class=\"btn btn-danger\">"+a.getName()+"</h5>"
+"<input type=\"text\" class=\"card-title\" value="+a.getName()+""+" name=\"nom"+compteur+"\" readonly  style=\"display:none;\">"+"</input>"
+"<label for=\"Price\">"+"Description produit :"+"</label>"
+"<h6>"+a.getDesc()+"</h6>"
+"<input type=\"text\" class=\"card-text\" value="+a.getDesc()+""+"  name=\"desc"+compteur+"\" readonly style=\"display:none;\">"+"</input>"
+"<label for=\"Price\">"+"Prix unitaire :"+"</label>"
+"<h6>"+a.getPrice()+"</h6>"
+"<input type=\"text\" class=\"card-text\" value="+a.getPrice()+""+" name=\"price"+compteur+"\"readonly style=\"display:none;\">"+"</input>"
+"<label for=\"Price\">"+"Quantitée :"+"</label>"
+"<input type=\"nubmer\" class=\"card-text\" value="+0+""+" name=\"quantity"+compteur+"\">"+"</input>"
+"<input type=\"submit\" class=\"customBtn\" value=\"Ajouter au panier\" style=\"margin:1em;\">"+"</input>"
+"</form>"
+"</div>"
+"</div>";
out.print(card);
compteur++;
}
%>
    </div>
    <div class="col-sm colPanierDynamique" syle="display:block;">
<%
String cardPanierTop = "<div class=\"card position-fixed\" style= \"margin-top: 2em;\">"
		+"<div class=\"card-body \">"
		+"<form class=\"panierDynamique\" action=\"panierserv\" method=\"post\" >"
		+"<ul class=\"marginLeft2Em noList btn btn-danger\"><li >"+"Mon Panier"+"</li></ul>";
		for(Article a : listPanier)
		{
			cardPanierRow += "<ul class=\"ulFlexRownSpaceEvenly noList\"><li >"+a.getName()+"</li><li >"+a.getQuantity()+"</li><li >"+a.getPrice()+"</li></ul>";
			
		}
String cardPanierBottom =
		"<ul class=\"btn btn-danger marginLeft2Em\"\">"+"Prix total en euros : "+prixTotal+"</ul>"
		+"<a type=\"submit\" class=\"customBtn marginLeft2Em\" href=\"panierserv\">"+"Passer au payment"+"</a>"
		+"</form>"
		+"</div>"
		+"</div>";
		
out.print(cardPanierTop);
out.print(cardPanierRow);
out.print(cardPanierBottom);
%>    
    </div>
  </div>
</div>
<jsp:directive.include file="/WEB-INF/template/footer.jsp" />
</body>
</html>