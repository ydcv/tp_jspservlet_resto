
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:directive.include file="/WEB-INF/template/css.jsp" />
<title>Insert title here</title>
</head>
<body>
<jsp:directive.include file="/WEB-INF/template/header.jsp" />
<%	
ArrayList<Article> liste = (ArrayList<Article>)request.getAttribute("listePanier");
Panier panier = (Panier) request.getAttribute("totalPanier");

String ArticleRow= "";

	String topRow =
	"<div class=\"container panierFlexDisplay\">"
	+"<h3>Le prix total de votre commande est de : "+panier.getPrixTotal()+" Euros"+"</h3>"
    +"<form action=\"panierserv\" method=\"post\" class=\"form-group\">"
    +"<div class=\"input-group mb-2\">"
    	+"<table>"
    	+" <thead>"
    	+"<tr>"
    	+"</tr>"
       	+"</thead>"
        +"<th>Réference article</th>"
		+"<th>Designation article</th>"
        +"<th>Quantité article</th>"
		+"<th>Prix article</th>"
        +"<tbody>";
        
    	for(Article a : liste){
    		ArticleRow += "<tr>"
            +"<td>"+a.getId()+"</td>"
            +"<td>"+a.getName()+"</td>"
            +"<td>"+a.getQuantity()+"</td>"
			+"<td>"+a.getPrice()+"</td>"
        	+"</tr>";
    	}
    	
    String bottomRow = 
    "</tbody>"
    +"</table>"
    +"</div>"
    +"<br>"
    +"<input type=\"submit\" value=\"   Valider la commande   \" class=\"btn btn-danger btn-lg btn-block\" style=\"margin-left:6em;\" />"
    +"</div>"
    +"</form>"
    +"</div>";
out.print(topRow);
out.print(ArticleRow);
out.print(bottomRow);

%> 

<jsp:directive.include file="/WEB-INF/template/footer.jsp" />
</body>
</html>